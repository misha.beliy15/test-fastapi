FROM python:3.8

WORKDIR /wheels
COPY "./src/requirements*.txt" /wheels/requirements/

RUN pip install -U pip && pip install -r /wheels/requirements/requirements.txt

ARG APP_DIR

COPY ./src "${APP_DIR}/"
COPY entrypoint /

WORKDIR "${APP_DIR}/"

ENTRYPOINT /entrypoint
